﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour {

    public float rotateSpeed = 10.0f;
    //public Quaternion localRotation;
    //private Vector3 z;

	// Use this for initialization
	void Start () {
        transform.rotation = Quaternion.identity;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        transform.Rotate(Vector3.forward * Time.deltaTime * rotateSpeed);


        /*Quaternion rotR = Quaternion.AngleAxis(10 * Time.deltaTime, Vector3.right);
        Quaternion rotU = Quaternion.AngleAxis(10 * Time.deltaTime, Vector3.up);

        // rotate around World Right
        transform.rotation = rotR * transform.rotation;
        // rotate around Local Up
        transform.rotation = transform.rotation * rotU; */
    }
}
