﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour {

    public Transform target;
    public float rotateSpeed = 5;
    Vector3 offset;

    Transform thisTransform;

	// Use this for initialization
	void Start () {
        //offset = target.transform.position - transform.position;
        Cursor.visible = false;
	}
	
	// Update is called once per frame
	void Update () {
        float x = Input.GetAxis("Mouse X") * rotateSpeed;
        float y = Input.GetAxis("Mouse Y") * rotateSpeed;
        target.Rotate(Vector3.up, -x);
        target.Rotate(Vector3.right, y);

        transform.LookAt(target.transform);
    }
}
