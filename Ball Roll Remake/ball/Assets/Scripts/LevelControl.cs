﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelControl : MonoBehaviour {

    public GameObject CurrentLevel;
    public GameObject NextLevel;
    public GameObject Player;

    private Vector3 PlayerStartPos;

	// Use this for initialization
	void Start () {
        PlayerStartPos = Player.transform.position;
	}
	
	// Update is called once per frame
	void OnCollisionEnter (Collision col) {
		Debug.Log ("Yo");
        CurrentLevel.SetActive(false);
        NextLevel.SetActive(true);
        Player.transform.position = PlayerStartPos;
        col.rigidbody.velocity = Vector3.zero;

	}
}
