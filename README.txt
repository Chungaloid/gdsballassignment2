Mack Homann and I (Chantel Eagle) worked on this project together.

Move the mouse to control the camera, WASD or arrowkeys to tilt the platforms, and spacebar to make the ball jump.

Our creative goals for the extended game was to create a sunny, cute, and colour game accompanied with upbeat music. Aesthetically we 
got inspiration from the Monkey Ball games, where the platforms were decorated with palm trees and big bananas! (Some Monkey Ball 
levels even took place over the ocean, so finding a free ocean skybox which happened to fit the theme was a big plus!). Monkey Ball had
a semi low-polly look (being an older game) and the assets I found in the asset store delivered a very similar look and feel.

	Unity Assets:
	FREE Food Pack - LUMO-ART 3D - https://twitter.com/LumoArt3D
	Low Polygod Soccer Ball - AHMET GENCOGLU - https://connect.unity.com/u/58b6f41932b306003178befe
	TGU Skybox Pack - BABYCAKE STUDIO - https://luiztelesfineart.wordpress.com/
	Cartoon Palmtree and Umb - 8BULL - http://alanalvarezb.wixsite.com/porfolio
	
	Music:
	https://freesound.org/people/InspectorJ/sounds/400632/
	https://freesound.org/people/leswagcupcakes/sounds/248144/

Parts of code were borrowed and changed from Luke Freeman-B. We also worked with people in class to build up our code. Online video
tutorials were also used to figure out how to make the moving platforms. Probuilder was used to build all the extended level platforms.

	https://www.youtube.com/watch?v=G4Ja-asl8hQ <-- this was the YT tutorial I referenced for platform animation.

Being the first time either of us had used Unity, we learned a lot but also encountered lots of problems. I handmade the two extended
levels myself and am happy with how they turned out, both functioally and aesthetically. Of course if I had more time I'd add more levels 
and maybe jazz them up a little more but overall I'm happy. One of the major problems we encoutered was figuring out how to make the portals
work. When I went over to Mack's house to work on the project at his place, I had the portals working perfectly. Once we uploaded it to
Google Drive and downloaded it onto my laptop, it somehow broke. We've asked friends for help, compared settings and troubleshooted but 
for the life of me I could not figure it out. 



